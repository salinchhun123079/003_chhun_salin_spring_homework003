package com.example.hw003.exception;

public class BookNotFoundException extends RuntimeException{
    public BookNotFoundException(Integer bookId){

        super("Book with id "+ bookId + "not found");
    }
}
