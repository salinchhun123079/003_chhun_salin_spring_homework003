package com.example.hw003.exception;

public class AuthorNotFoundException extends RuntimeException{
    public AuthorNotFoundException(Integer authorId){
        super("User with id "+ authorId + "not found");
    }
}
