package com.example.hw003.exception;

import com.example.hw003.model.entity.Author;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.net.URI;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.time.Instant;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(AuthorNotFoundException.class)
    ProblemDetail handlerAuthorNotFoundException(AuthorNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("Author not found");
        problemDetail.setDetail("Author with id: not found");
        problemDetail.setProperty("user",AuthorNotFoundException.class);
        problemDetail.setProperty("Timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/users/not-found"));
        return problemDetail;
    }

    @ExceptionHandler(BookNotFoundException.class)
    ProblemDetail handlerBookNotFoundException(BookNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("Book not found");
        problemDetail.setDetail("Book with id: not found");
        problemDetail.setProperty("user",BookNotFoundException.class);
        problemDetail.setProperty("Timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/bopks/not-found"));
        return problemDetail;
    }

    @ExceptionHandler(CategoryFoundException.class)
    ProblemDetail handlerCategoryNotFoundException(CategoryFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("Category not found");
        problemDetail.setDetail("Category with id: not found");
        problemDetail.setProperty("user",CategoryFoundException.class);
        problemDetail.setProperty("Timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/bopks/not-found"));
        return problemDetail;
    }

}
