package com.example.hw003.services;

import com.example.hw003.model.entity.Category;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.model.request.CategoryRequest;

import java.util.List;
public interface CategoryService {
    List<Category> getAllCategories();

    Category getCategoryById(Integer categoryId);

    boolean deleteCategoryById(Integer categoryId);

    Integer insertCategory(CategoryRequest categoryRequest);

    Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId);
}
