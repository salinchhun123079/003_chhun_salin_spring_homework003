package com.example.hw003.services;

import com.example.hw003.model.entity.Book;
import com.example.hw003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();

    Book getBookById(Integer bookId);

    boolean deleteBookById(Integer bookId);

    Integer insertBook(BookRequest bookRequest);

}
