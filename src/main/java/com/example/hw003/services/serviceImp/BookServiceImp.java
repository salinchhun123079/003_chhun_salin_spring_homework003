package com.example.hw003.services.serviceImp;

import com.example.hw003.exception.AuthorNotFoundException;
import com.example.hw003.exception.BookNotFoundException;
import com.example.hw003.model.entity.Book;
import com.example.hw003.model.request.BookRequest;
import com.example.hw003.repository.BookRepository;
import com.example.hw003.services.BookService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImp implements BookService {
    private final BookRepository bookRepository;

    public BookServiceImp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Override
    public List<Book> getAllBook() {
        return bookRepository.findAllBook();
    }
    Book book= new Book();
    @Override
    public Book getBookById(Integer bookId) {
        book = bookRepository.getBookById(bookId);
        if (book!=null){
            return book;
        }else throw new BookNotFoundException(bookId);
    }

    @Override
    public boolean deleteBookById(Integer bookId) {
        if (book!=null){
            return bookRepository.deleteBookById(bookId);
        }else throw new AuthorNotFoundException(bookId);
    }

//    @Override
//    public Integer insertBook(BookRequest bookRequest) {
//        Integer bookId = bookRepository.insertBook(bookRequest);
//        return bookId;
//    }
    @Override
    public Integer insertBook(BookRequest bookRequest) {
        Integer bookId = bookRepository.insertBook(bookRequest);
        for(int i = 0; i<bookRequest.getCategoriesId().size(); i++){

            bookRepository.addCategoryIdToBookTable(bookId, bookRequest.getCategoriesId().get(i));

        }
        return bookRepository.insertBook(bookRequest);
    }


}
