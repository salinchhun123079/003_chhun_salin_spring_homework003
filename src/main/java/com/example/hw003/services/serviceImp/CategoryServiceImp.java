package com.example.hw003.services.serviceImp;

import com.example.hw003.exception.CategoryFoundException;
import com.example.hw003.model.entity.Category;
import com.example.hw003.model.request.CategoryRequest;
import com.example.hw003.repository.CategoryRepository;
import com.example.hw003.services.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {
    private final CategoryRepository categoryRepository;
    Category category= new Category();

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAllCategories();
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        category = categoryRepository.getCategoryById(categoryId);
        if (category!=null){
            return category;
        }else throw new CategoryFoundException(categoryId);
    }

    @Override
    public boolean deleteCategoryById(Integer categoryId) {
        return categoryRepository.deleteCategoryById(categoryId);
    }

    @Override
    public Integer insertCategory(CategoryRequest categoryRequest) {
        Integer categoryId = categoryRepository.insertCategory(categoryRequest);
        return categoryId;
    }

    @Override
    public Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId) {
        return categoryRepository.updateCategory(categoryRequest,categoryId);
    }


}
