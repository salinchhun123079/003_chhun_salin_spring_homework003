package com.example.hw003.services.serviceImp;

import com.example.hw003.exception.AuthorNotFoundException;
import com.example.hw003.model.entity.Author;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.repository.AuthorRepository;
import com.example.hw003.services.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    Author author= new Author();

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.findAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        author = authorRepository.getAuthorById(authorId);
        if (author!=null){
            return author;
        }else throw new AuthorNotFoundException(authorId);

    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
//        author = authorRepository.deleteAuthorById(authorId);
        if (author!=null){
            return authorRepository.deleteAuthorById(authorId);
        }else throw new AuthorNotFoundException(authorId);

//
    }

    @Override
    public Integer insertAuthor(AuthorRequest authorRequest) {
        Integer authorId = authorRepository.insertAuthor(authorRequest);
        return authorId;
    }

    @Override
    public Integer updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        Integer authorIdUpdate = authorRepository.updateAuthor(authorRequest,authorId);
        return authorIdUpdate;
    }


}
