package com.example.hw003.repository;

import com.example.hw003.model.entity.Author;
import com.example.hw003.model.entity.Category;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface CategoryRepository {
//    @Select("SELECT ct.name " +
//            "FROM categories ct " +
//            "INNER JOIN book_details bct ON ct.category_id = bct.category_id "+
//            "WHERE bct.book_id = #{bookId}")
//    List<Category> getCategoryByBookId(Integer bookId);
//

    @Select("SELECT * FROM categories")
    List<Category> findAllCategories();

    @Select("SELECT * FROM categories WHERE category_id = #{categoryId}")
    Category getCategoryById(Integer categoryId);


    @Delete("DELETE FROM categories WHERE category_id = #{categoryId}")
    boolean deleteCategoryById(@Param("categoryId") Integer categoryId);

    @Select("INSERT INTO categories (name) VALUES(#{request.name}) RETURNING category_id")
    Integer insertCategory(@Param("request") CategoryRequest categoryRequest);

    @Select("UPDATE categories " +
            "SET name = #{request.name} " +
            "WHERE category_id = #{categoryId} " +
            "RETURNING category_id")
    Integer updateCategory(@Param("request") CategoryRequest categoryRequest, Integer categoryId);
}
