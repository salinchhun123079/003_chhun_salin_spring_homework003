package com.example.hw003.repository;

import com.example.hw003.model.entity.Book;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {

//get all book
    @Select("SELECT * FROM book")
    @Results(
            id = "bookMapper",
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "publishDate", column = "import_date"),
                    @Result(property = "author", column = "author_id",
                            one = @One(select = "com.example.hw003.repository.AuthorRepository.getAuthorById")
                    ),

                    @Result(property = "categories", column = "category_id",
                            many = @Many(select="com.example.hw003.repository.CategoryRepository.getCategoryByBookId")
                    )
            }
    )

    List<Book> findAllBook();

//get book by id
    @Select("SELECT * FROM book WHERE id =#{booKId}")
    @ResultMap("bookMapper")
    Book getBookById(Integer bookId);

//delete book by id
    @Delete("DELETE FROM book WHERE id = #{authorId}")
    boolean deleteBookById(@Param("authorId") Integer bookId);

//insert book
    @Select("INSERT INTO book (title, import_date, author_id) " +
            "VALUES(#{book.title}, #{book.import_date}, #{book.author_id}) " +
            "RETURNING id")
    Integer insertBook(@Param("book") BookRequest bookRequest);

    @Insert("INSERT INTO book_details(book_id, category_id) " +
            "VALUES(#{bookId}, #{categoryId})")
    void addCategoryIdToBookTable(Integer bookId, Integer categoryId);
}
