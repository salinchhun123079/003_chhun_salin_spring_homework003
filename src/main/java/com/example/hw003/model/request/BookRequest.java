package com.example.hw003.model.request;

import com.example.hw003.model.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookRequest {

    private String title;
    private LocalDateTime import_date;
    private Integer author_id;
    private List<Integer>categoriesId;
}

