package com.example.hw003.controller;

import com.example.hw003.model.entity.Author;
import com.example.hw003.model.entity.Book;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.model.request.BookRequest;
import com.example.hw003.model.response.AuthorResponse;
import com.example.hw003.services.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    private final BookService bookService;
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all books")
    public ResponseEntity<AuthorResponse<List<Book>>> getAllBook(){
        AuthorResponse<List<Book>>response=AuthorResponse.<List<Book>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched books")
                .payload(bookService.getAllBook())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{bookId}")
    @Operation(summary = "Get book by id")
    public ResponseEntity<?> getBookById(@PathVariable Integer bookId) {
        AuthorResponse<Book> response = AuthorResponse.<Book>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .message("Successfully fetched authors by id")
                .payload(bookService.getBookById(bookId))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("{bookId}")
    @Operation(summary = "Delete book by id")
    public ResponseEntity<AuthorResponse<String>> deleteBookById(@PathVariable Integer bookId) {
        AuthorResponse<String> response = null;
        if (bookService.deleteBookById(bookId) == true) {
            response = AuthorResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully deleted book")
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/add new book")
    @Operation(summary = "Insert new book")
    public ResponseEntity<AuthorResponse<Book>> insertBook(@RequestBody BookRequest bookRequest){
        Integer bookId = bookService.insertBook(bookRequest);
        AuthorResponse<Book> response =AuthorResponse.<Book>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully added author")
                .payload(bookService.getBookById(bookId))
                .build();
        return ResponseEntity.ok(response);

//                    .status(200)
//                    .message("Successfully added author")
//                    .payload(bookService.getBookById(storeBookId))
//                    .build();
//            return ResponseEntity.ok(response);
//                    .timestamp(new Timestamp(System.currentTimeMillis()))
//                    .status(200)
//                    .message("Successfully added author")
//                    .payload(bookService.getBookById(storeBookId))
//                    .build();
//            return ResponseEntity.ok(response);
//        if (storeBookId != null){
//            AuthorResponse<Book> response =AuthorResponse.<Book>builder()
//                    .timestamp(new Timestamp(System.currentTimeMillis()))
//                    .status(200)
//                    .message("Successfully added author")
//                    .payload(bookService.getBookById(storeBookId))
//                    .build();
//            return ResponseEntity.ok(response);
//        }
    }


}
