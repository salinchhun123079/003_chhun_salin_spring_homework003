CREATE TABLE authors (
    id SERIAL PRIMARY KEY ,
    name VARCHAR(255),
    gender VARCHAR(10)
);

CREATE TABLE categories(
    category_id SERIAL PRIMARY KEY ,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE book(
    id SERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL ,
    import_date TIMESTAMP,
    author_id INT REFERENCES authors(id)
);

CREATE TABLE book_details(
    book_id INT REFERENCES book(id) ON UPDATE CASCADE ON DELETE CASCADE ,
    category_id INT REFERENCES categories(id) ON UPDATE CASCADE ON DELETE CASCADE ,
    PRIMARY KEY (book_id, category_id)
    );